package com.lsw.pcmanager.controller;

import com.lsw.pcmanager.model.PcRequest;
import com.lsw.pcmanager.service.PcService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pc")
public class PcController {

    private final PcService pcService;

    @PostMapping("data")
    public String setPc(@RequestBody PcRequest request) {
        pcService.setPc(request.getPcName(),request.getHumanName());
        return "OK";
    }
}
