package com.lsw.pcmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcRequest {

    private String pcName;

    private String humanName;

}
