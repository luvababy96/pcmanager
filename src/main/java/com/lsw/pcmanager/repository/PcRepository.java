package com.lsw.pcmanager.repository;

import com.lsw.pcmanager.entity.Pc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcRepository extends JpaRepository<Pc,Long> {
}
