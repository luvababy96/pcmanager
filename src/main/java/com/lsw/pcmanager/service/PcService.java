package com.lsw.pcmanager.service;

import com.lsw.pcmanager.entity.Pc;
import com.lsw.pcmanager.repository.PcRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PcService {

    private final PcRepository pcRepository;

    public void setPc(String pcName, String humanName) {
        Pc addData = new Pc();
        addData.setPcName(pcName);
        addData.setHumanName(humanName);

        pcRepository.save(addData);
    }
}
